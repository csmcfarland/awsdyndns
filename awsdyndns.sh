#!/usr/bin/env bash

# Copyright 2018 Cameron S. McFarland

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <https://www.gnu.org/licenses/>.

# update53 <record name> <type> <value>
function update53 {

json=$(cat <<EOF
{
  "Comment": "Dynamic Update",
  "Changes": [
    {
      "Action": "UPSERT",
      "ResourceRecordSet": {
        "Name": "${1}",
        "Type": "${2}",
        "TTL": 300,
        "ResourceRecords": [
          {
            "Value": "${3}"
          }
        ]
      }
    }
  ]
}
EOF
)

# echo ${json}
echo "Updating ${1} record type ${2} to ${3} in zone ${4}."
/usr/local/bin/aws route53 change-resource-record-sets --hosted-zone-id "${4}" --change-batch "${json}"

}

ipv6regex="((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))"
ipv4regex="^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$"

DNSSERVER="8.8.8.8"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -r|--record)
    RECORD="$2"
    shift # past argument
    shift # past value
    ;;
    -s|--server)
    DNSSERVER="$2"
    shift # past argument
    shift # past value
    ;;
    -z|--zid)
    ZONEID="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [ -z "$RECORD" ] || [ -z "$ZONEID" ]; then
  echo "Usage: ${0} -r dns.example.com -s 8.8.8.8 -z AAAAAAAAAAAAA"
  exit 1
fi

## Get our current IP addresses:
IPV4=$(/usr/bin/curl -4 -s https://ifconfig.io)
IPV6=$(/usr/bin/curl -6 -s https://ifconfig.io)
## Get our current DNS settings:
GETIPV4=$(/usr/bin/dig +short -t A ${RECORD} @${DNSSERVER})
GETIPV6=$(/usr/bin/dig +short -t AAAA ${RECORD} @${DNSSERVER})

# echo "${IPV4} : ${GETIPV4}"
# echo "${IPV6} : ${GETIPV6}"

if ! [[ $IPV4 =~ $ipv4regex ]] && [[ $IPV4 != "" ]]; then
  echo "${IPV4} seems like a bad response for an IPV4 address."
  IPV4=""
fi

if ! [[ $IPV6 =~ $ipv6regex ]] && [[ $IPV6 != "" ]]; then
  echo "${IPV6} seems like a bad response for an IPV6 address."
  IPV6=""
fi

## If our current IPv4 address is not empty and does not match DNS, update it.
if [ "${IPV4}" != "" ] && [ "${IPV4}" != "${GETIPV4}" ]; then
  # echo "Updating ${RECORD} with ${IPV4}"
  update53 "${RECORD}" "A" "${IPV4}" "${ZONEID}"
fi

## If our current IPv6 address is not empty and does not match DNS, update it.
if [ "${IPV6}" != "" ] && [ "${IPV6}" != "${GETIPV6}" ]; then
  # echo "Updating ${RECORD} with ${IPV6}"
  update53 "${RECORD}" "AAAA" "${IPV6}" "${ZONEID}"
fi
