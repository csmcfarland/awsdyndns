# awsdyndns.sh
This is a pretty simple bash script that will update an IPv4 and/or IPv6 ip address in Route 53. My goal was to write something that would let me leave other dynamic DNS providers and use AWS exclusively.

This script assumes you have AWS cli installed and configured with an account that can manipulate Route 53 zones.

If you want to change the default TTL, you should edit the json section to update the TTL number. I have it set to five minutes now, but something less aggressive would also work.

# Usage
```
awsdyndns.sh -r <record name> [-s DNS server ip]
```

The record name you provide should be the name you want to update. The default DNS server ip is using one of Google's DNS servers (8.8.8.8), but you can specify your own.

Examples:
> bash awsdyndns.sh -r www.example.com

> bash awsdyndns.sh -r *.example.com -s 1.1.1.1
